﻿using MongoDB.Driver;
using MongoTest.Models;
using System;

namespace MongoTest
{
    class Program
    {
        static void Main(string[] args)
        {
            const string CONNECTION_STRING = "mongodb://127.0.0.1:27017/";
            const string DATABASE_NAME = "mongoTest";

            var client = new MongoClient(CONNECTION_STRING);
            var db = client.GetDatabase(DATABASE_NAME);
            bool exit = true;

            while (exit)
            {
                Console.WriteLine("\n1 - add temperature \n" +
                  "2 - read all temperatures \n" +
                  "3 - read temp by ID \n" +
                  "4 - update temp \n" +
                  "5 - delete all temperatures \n" +
                  "6 - delete temp \n" +
                  "201 - add temperature with address \n" +
                  "100 - exit");

                var userChoose = Console.ReadLine();
                Console.Clear();

                if (userChoose == "1")
                {
                    var collection = db.GetCollection<Temperature>("temperatures");
                    AddTemperatures(collection);
                }
                else if (userChoose == "201")
                {
                    var collection = db.GetCollection<Temperature>("temperatures");
                    Console.WriteLine("Podaj nazwę miasta");
                    var city = Console.ReadLine();

                    AddTemperaturesWithAddress(collection, city);
                }
                else if (userChoose == "2")
                {
                    var collection = db.GetCollection<Temperature>("temperatures");
                    ReadAllTemperatures(collection);
                }
                else if (userChoose == "3")
                {
                    Console.WriteLine("Podaj ID");
                    var id = Console.ReadLine();
                    var collection = db.GetCollection<Temperature>("temperatures");

                    ReadTemperature(collection, id);
                }
                else if (userChoose == "4")
                {
                    Console.WriteLine("Podaj ID");
                    var id = Console.ReadLine();
                    Console.WriteLine("Podaj temp.");
                    var temp = Console.ReadLine();

                    var collection = db.GetCollection<Temperature>("temperatures");

                    UpdateTemperature(collection, id, temp);
                }
                else if (userChoose == "5")
                {
                    var collection = db.GetCollection<Temperature>("temperatures");

                    DeleteAllTemperatures(collection);
                }
                else if (userChoose == "6")
                {
                    Console.WriteLine("Podaj ID");
                    var id = Console.ReadLine();
                    var collection = db.GetCollection<Temperature>("temperatures");

                    DeleteTemperature(collection, id);
                }
                else if (userChoose == "100")
                {
                    exit = false;
                }
                else
                {
                    Console.WriteLine("Nie znaleziono");
                }
            }
        }

        private static void AddTemperatures(IMongoCollection<Temperature> collection)
        {
            var rnd = new Random();

            var temp = new Temperature
            {
                Date = DateTime.Now,
                Humidity = rnd.Next(30, 100),
                Temp = rnd.Next(-30, 39)
            };

            collection.InsertOne(temp);

            Console.WriteLine("Temparture added");
        }

        private static void AddTemperaturesWithAddress(IMongoCollection<Temperature> collection, string city)
        {
            var rnd = new Random();

            var temp = new Temperature
            {
                Date = DateTime.Now,
                Humidity = rnd.Next(30, 100),
                Temp = rnd.Next(-30, 39),
                Address = new Address
                {
                    City = city
                }
            };

            collection.InsertOne(temp);

            Console.WriteLine("Temparture added");
        }

        private static void ReadAllTemperatures(IMongoCollection<Temperature> collection)
        {
            var tempList = collection.Find(t => true).ToList();

            tempList.ForEach(t => Console.WriteLine($"{t.Date} - {t.Temp} °C"));
        }

        private static void ReadTemperature(IMongoCollection<Temperature> collection, string id)
        {
            try
            {
                var temp = collection.Find(t => t.Id == id).FirstOrDefault();

                if (temp == null)
                {
                    Console.WriteLine("Brak danych");

                    return;
                }

                Console.WriteLine($"{temp.Date} - {temp.Temp} °C");
            }
            catch (FormatException)
            {
                Console.WriteLine("Wrong ID format");
            }
        }

        private static void UpdateTemperature(IMongoCollection<Temperature> collection, string id, string tempValue)
        {
            try
            {
                var update = Builders<Temperature>.Update.Set(t => t.Temp, int.Parse(tempValue));
                var result = collection.UpdateOne(t => t.Id == id, update);

                Console.WriteLine("Temp updated");
            }
            catch (FormatException)
            {
                Console.WriteLine("Wrong ID format");
            }
        }

        private static void DeleteAllTemperatures(IMongoCollection<Temperature> collection)
        {
            var deleteResult = collection.DeleteMany(t => true);

            Console.WriteLine($"Usunięto {deleteResult.DeletedCount} dokumentów");
        }

        private static void DeleteTemperature(IMongoCollection<Temperature> collection, string id)
        {            
            try
            {
                var deleteResult = collection.DeleteOne(t => t.Id == id);

                Console.WriteLine($"Usunięto {deleteResult.DeletedCount} dokumentów");
            }
            catch (FormatException)
            {
                Console.WriteLine("Wrong ID format");
            }
        }
    }
}
