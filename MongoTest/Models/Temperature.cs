﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MongoTest.Models
{
    [BsonIgnoreExtraElements]
    public class Temperature
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Day")]
        public DateTime Date { get; set; }
        public double Temp { get; set; }
        public int Humidity { get; set; }
        public Address Address { get; set; }
    }
}
