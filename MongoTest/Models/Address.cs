﻿using MongoDB.Bson.Serialization.Attributes;

namespace MongoTest.Models
{
    [BsonIgnoreExtraElements]
    public class Address
    {
        public string City { get; set; }
    }
}
